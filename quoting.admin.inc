<?php

/**
 * @file
 *  Administration callbacks, forms and helpers
 */

/**
 * Settings page. Menu entry for admin/settings/quoting
 */
function quoting_admin($form_state) {
  $form = array();

  $form['content-types'] = array(
    '#type' => 'fieldset',
    '#title' => t('Content Types'),
    '#collapsible' => TRUE,
  );  

  // Get types
  $types = node_get_types();
  $options = array();
  foreach ($types as $type => $object) {
    $options[$type] = $object->name;
  }

  $form['content-types']['quoting_types'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Types to be quoted'),
    '#description' => t('Content types that you want to be quoted. The content type form will provide important additional features for each content type.'),
    '#options' => $options,
    '#default_value' => variable_get('quoting_types', array()),
  );

  array_unshift($options, t('<< None >>'));
  $form['content-types']['quote_type'] = array(
    '#type' => 'select',
    '#title' => t('Quote Type'),
    '#description' => t('Content type that you want to be the quote metadata.'),
    '#options' => $options,
    '#default_value' => variable_get('quote_type', 0),
  );

  return system_settings_form($form);
}


