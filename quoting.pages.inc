<?php

/**
 * @file
 *  Callbacks for auxiliary pages
 */

/**
 * Callback for quoting an item
 */
function quoting_quote_item_page($node) {
  global $user;

  $quote_item_types = variable_get('quote_type_'. $node->type, array());

  if (empty($quote_item_types)) {
    drupal_goto('node/'. $node->nid);
  }

  if (count($quote_item_types) == 1) {
    $quote_item_type = current($quote_item_types);
  }
  else {
    $quote_item_type = db_result(db_query('SELECT quote_type FROM {quoting_quote_type} WHERE nid = %d', $node->nid));
  }
  $qtype = node_get_types('type', $quote_item_type);
  drupal_set_title(t('Quoting '. $node->title));

  // Make a new node and loads the node form:
  module_load_include('inc', 'node', 'node.pages');
  $quote_item = array(
    'uid' => $user->uid, 
    'name' => (isset($user->name) ? $user->name : ''), 
    'type' => $qtype->type, 
    'language' => '',
  );

  $form_state = array();
  return drupal_get_form($qtype->type . '_node_form', $quote_item);
}

/**
 * Quote finish page
 */
function quoting_quote_page() {
  global $user;
  $content = '';

  $quote_type = variable_get('quote_type', 0);

  if (!$quote_type) {
    drupal_goto('<front>');
  }

  // Load quoting items so far.
  $items = quoting_restore_quote_items();
  if (empty($items)) {
    drupal_set_message(t('No items added to quote yet.'), 'error');
    return '';
  }
  $content .= theme('quoting_item_list', $items);

  // Make a new node and loads the node form:
  module_load_include('inc', 'node', 'node.pages');
  $quote = array(
    'uid' => $user->uid, 
    'name' => (isset($user->name) ? $user->name : ''), 
    'type' => $quote_type, 
    'language' => '',
  );

  $form_state = array();
  $content .= drupal_get_form($quote_type . '_node_form', $quote);

  return $content;
}
