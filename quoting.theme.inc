<?php
// $Id 

/**
 * @file 
 *  theme functions
 */

/**
 * Themes a list of items already quoted.
 * @param
 *  items - check output of quoting_restore_quote_items()
 */
function theme_quoting_item_list($items) {
  $list = array();
  foreach ($items as $item) {
    $node = node_load($item['nid']);
    $quote_item = node_build_content($item['quote_item'], FALSE, FALSE);
    $list_item = l($node->title, 'node/'. $node->nid);
    $list_item .= '<div class="content">'. drupal_render($quote_item->content) . '</div>';
    $list[] = $list_item;
  }
  return '<div class="quoting-item-list">'. theme('item_list', $list) .'</div>';
}
