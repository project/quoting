<?php

/**
 * @file
 * This file handles views api definitions.
 * We describe the table to Views module 
 * as well as other necessary stuff to integrate
 */

/**
 * Implementation of hook_views_data()
 */
function quoting_views_data(){
  $data = array();

  // quoting table

  // Our group in Views UI
  $data['quoting']['table']['group'] = t('Quoting');

  // Our join types. 
  $data['quoting']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'user' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
    'sessions' => array(
      'left_field' => 'sid',
      'field' => 'sid',
    ),
  );

  $data['quoting']['qid'] = array(
    'title' => t('Quote ID'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
      'click sortable' => FALSE,
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_boolean_operator',
      'label' => t('Is quote'),
    ),
  );

  $data['quoting']['nid'] = array(
    'title' => t('Quote metadata'),
    'help' => t('The node with the quote metadata'),
    'relationship' => array(
      'label' => t('Quote metadata'),
      'base' => 'node',
      'base field' => 'nid',
    ),
  );

  // quoting_item table

  $data['quoting_item']['table']['group'] = t('Quoting Item');

  // Our join types. 
  $data['quoting_item']['table']['join'] = array(
    'node' => array(
      'left_field' => 'nid',
      'field' => 'nid',
    ),
    'user' => array(
      'left_field' => 'uid',
      'field' => 'uid',
    ),
    'sessions' => array(
      'left_field' => 'sid',
      'field' => 'sid',
    ),
    'quoting' => array(
      'left_field' => 'qid',
      'field' => 'qid',
    ),
  );

  return $data;
}
